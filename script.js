"use strict" ;

// 1. В чому відмінність між setInterval та setTimeout?

// setInterval - використовується для виконання функції або коду через певний інтервал часу який встановлюється, функція запускається безперервно.
// setTimeout - використовується для виконання функції або коду один раз після певного часу затримки, запускається тільки один раз.

// 2. Чи можна стверджувати, що функції в setInterval та setTimeout будуть виконані рівно через той проміжок часу, який ви вказали?

// Ні, це пов'язано з тим, що JavaScript є однопоточною мовою, і виконання коду може бути вплинуте іншими факторами, такими як навантаження на процесор,
//  події в браузері та інші виклики від користувача.

// 3. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?

// можна скористатися методом clearTimeout або clearInterval.

// setTimeout можна припинити, викликавши clearTimeout та передавши ідентифікатор таймера
// setInterval можна припинити, викликавши clearInterval та передавши ідентифікатор інтервалу.

// -Створіть HTML-файл із кнопкою та елементом div.
// -При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди.
//  Новий текст повинен вказувати, що операція виконана успішно.

// const button = document.getElementById('Button');
// const div = document.getElementById('Div');

// button.addEventListener('click', () => {
//   setTimeout(() => {
//     div.textContent = 'Operation is successful!';
//   }, 3000);
// });


// Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. 
// Після досягнення 1 змініть текст на "Зворотній відлік завершено".

const Div = document.getElementById('Div');

function updateDiv(seconds) {
    Div.textContent = `Countdown: ${seconds}`;
}
function startDiv() {
  let seconds = 10;
  const interval = setInterval(() => {
    updateDiv(seconds);
    seconds--;
    if (seconds < 1) {
      clearInterval(interval);
      updateDiv('The countdown is over');
    }
  }, 1000);
}
startDiv();